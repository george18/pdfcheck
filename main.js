let $board = $(".board");
let $tools = $(".toolbar .tools");
let $coordinateX = $("#coordinate-x");
let $coordinateY = $("#coordinate-y");
let $width = $("#width");
let $height = $("#height");
let $backgroundColor = $("#background-color");
let $borderColor = $("#border-color");
let $borderWidth = $("#border-width");

let makeActive = function ($component) {

};

let defaultCheckboxOptions = {
    width: 50,
    height: 50,
};

let activeControls = (function () {
    let $activeComponent = null;

    let activeExists = function () {
        return $activeComponent !== null;
    };

    let getActiveComponent = function () {
        return $activeComponent;
    };

    let setActive = function ($component) {
        removeActive();
        $activeComponent = $component;
        $activeComponent.addClass("active");
        showActiveToolboardWithComponent($component);
    };

    let removeActive = function () {
        if ($activeComponent !== null) {
            $activeComponent.removeClass("active");
            $activeComponent = null;
            $tools.hide();
        }
    };

    return {
        activeExists: activeExists,
        getActiveComponent: getActiveComponent,
        setActive: setActive,
        removeActive: removeActive
    }
})();

let componentMouseDownEvent = function (e) {
    let mouseMovedX = 0;
    let mouseMovedY = 0;
    let currentMouseX = e.clientX;
    let currentMouseY = e.clientY;

    e.preventDefault();
    e.stopImmediatePropagation();

    let mouseMoveHandler = (e) => {
        let left = parseFloat($(this).css("left"));
        let top = parseFloat($(this).css("top"));

        let mouseMovedX = e.clientX - currentMouseX + left;
        let mouseMovedY = e.clientY - currentMouseY + top;

        console.log({
            mouseMovedX,
            mouseMovedY,
            currentMouseX,
            currentMouseY,
            left,
            top,
            clientX: e.clientX,
            clientY: e.clientY
        });

        $(this).css("left", mouseMovedX);
        $(this).css("top", mouseMovedY);

        currentMouseX = e.clientX;
        currentMouseY = e.clientY;
    };

    $(document).on("mousemove", mouseMoveHandler);

    $(document).one("mouseup", function () {
        $(document).off("mousemove", mouseMoveHandler);
    });

    activeControls.setActive($(this));
};

let addEventsToComponent = function ($component) {
    $component.on("mousedown", componentMouseDownEvent);
    $component.on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
    });

    $component.find(".component-resize-controls .resize-control").on("click, mousedown", function (e) {
        e.preventDefault();
        e.stopPropagation();
    });

    $component.find(".component-resize-controls .resize-right").on("mousedown", function (e) {
        let currentMouseX = e.clientX;
        let currentMouseY = e.clientY;

        let $currentComponent = $(this).closest(".component");
        let $checkbox = $currentComponent.find(".checkbox");

        e.preventDefault();
        e.stopImmediatePropagation();

        let mouseMoveHandler = (e) => {
            let width = parseFloat($checkbox.width());
            // let height = parseFloat($currentComponent.height());

            let mouseMovedX = e.clientX - currentMouseX + width;
            // let mouseMovedY = e.clientY - currentMouseY + height;

            $checkbox.width(mouseMovedX);
            // $checkbox.height(mouseMovedY);

            currentMouseX = e.clientX;
            currentMouseY = e.clientY;
        };

        $(document).on("mousemove", mouseMoveHandler);

        $(document).one("mouseup", function () {
            $(document).off("mousemove", mouseMoveHandler);
        });
    });

    $component.find(".component-resize-controls .resize-left").on("mousedown", function (e) {
        let currentMouseX = e.clientX;
        let currentMouseY = e.clientY;

        let $currentComponent = $(this).closest(".component");
        let $checkbox = $currentComponent.find(".checkbox");

        e.preventDefault();
        e.stopImmediatePropagation();

        let mouseMoveHandler = (e) => {
            let left = parseFloat($currentComponent.css("left"));
            let width = parseFloat($checkbox.width());
            // let height = parseFloat($currentComponent.height());

            let mouseMovedX = e.clientX - currentMouseX + left;
            // let mouseMovedWidth = e.clientX + currentMouseX + width;
            let mouseMovedWidth = currentMouseX - e.clientX  + width;
            // let mouseMovedY = e.clientY - currentMouseY + height;

            // console.log({mouseMovedX});

            // width += mouseMovedX;

            $currentComponent.css("left", mouseMovedX);
            $checkbox.width(mouseMovedWidth);
            // $checkbox.width(mouseMovedX);
            // $checkbox.height(mouseMovedY);

            currentMouseX = e.clientX;
            currentMouseY = e.clientY;
        };

        $(document).on("mousemove", mouseMoveHandler);

        $(document).one("mouseup", function () {
            $(document).off("mousemove", mouseMoveHandler);
        });
    });


    $component.find(".component-resize-controls .resize-bottom").on("mousedown", function (e) {
        let currentMouseX = e.clientX;
        let currentMouseY = e.clientY;

        let $currentComponent = $(this).closest(".component");
        let $checkbox = $currentComponent.find(".checkbox");

        e.preventDefault();
        e.stopImmediatePropagation();

        let mouseMoveHandler = (e) => {
            // let width = parseFloat($checkbox.width());
            let height = parseFloat($checkbox.height());

            // let mouseMovedX = e.clientX - currentMouseX + width;
            let mouseMovedY = e.clientY - currentMouseY + height;

            $checkbox.height(mouseMovedY);
            // $checkbox.height(mouseMovedY);

            currentMouseX = e.clientX;
            currentMouseY = e.clientY;
        };

        $(document).on("mousemove", mouseMoveHandler);

        $(document).one("mouseup", function () {
            $(document).off("mousemove", mouseMoveHandler);
        });
    });

    $component.find(".component-resize-controls .resize-top").on("mousedown", function (e) {
        let currentMouseX = e.clientX;
        let currentMouseY = e.clientY;

        let $currentComponent = $(this).closest(".component");
        let $checkbox = $currentComponent.find(".checkbox");

        e.preventDefault();
        e.stopImmediatePropagation();

        let mouseMoveHandler = (e) => {
            // let left = parseFloat($currentComponent.css("left"));
            let top = parseFloat($currentComponent.css("top"));
            // let width = parseFloat($checkbox.width());
            let height = parseFloat($currentComponent.height());

            // let mouseMovedX = e.clientX - currentMouseX + left;
            // let mouseMovedWidth = e.clientX + currentMouseX + width;
            // let mouseMovedWidth = currentMouseX - e.clientX  + width;
            let mouseMovedHeight = currentMouseX - e.clientX  + height;
            let mouseMovedY = e.clientY - currentMouseY + top;

            // console.log({mouseMovedX});

            // width += mouseMovedX;

            $currentComponent.css("top", mouseMovedY);
            $checkbox.height(mouseMovedHeight);
            // $checkbox.width(mouseMovedX);
            // $checkbox.height(mouseMovedY);

            currentMouseX = e.clientX;
            currentMouseY = e.clientY;
        };

        $(document).on("mousemove", mouseMoveHandler);

        $(document).one("mouseup", function () {
            $(document).off("mousemove", mouseMoveHandler);
        });
    });
};

function showActiveToolboardWithComponent($component) {
    $tools.show();

    var $checkbox = $component.find(".checkbox");

    $coordinateX.val(parseInt($component.css("left")));
    $coordinateY.val(parseInt($component.css("top")));
    $width.val(parseInt($component.width()));
    $height.val(parseInt($component.height()));

    $backgroundColor.val($checkbox.css("backgroundColor"));
    $borderWidth.val(parseFloat($checkbox.css("borderWidth")));
}


$coordinateX.on("input", function () {

    let x = $(this).val();

    if (activeControls.activeExists()) {
        console.log( activeControls.getActiveComponent());
        activeControls.getActiveComponent().css("left", parseFloat(x));

        // activeControls.getActiveComponent().css("left", $(this).val());
    }
});

$coordinateY.on("input", function (e) {
    e.preventDefault();
    e.stopPropagation();

    if (activeControls.activeExists()) {
        activeControls.getActiveComponent().css("top", parseFloat($(this).val()));
    }
});

$width.on("input", function (e) {
    e.preventDefault();
    e.stopPropagation();

    if (activeControls.activeExists()) {
        activeControls.getActiveComponent().find(".checkbox").width($(this).val());
    }
});

$backgroundColor.on("input", function (e) {
    e.preventDefault();
    e.stopPropagation();

    if (activeControls.activeExists()) {
        activeControls.getActiveComponent().find(".checkbox").css("backgroundColor", $(this).val());
    }
});

$height.on("input", function (e) {
    e.preventDefault();
    e.stopPropagation();

    if (activeControls.activeExists()) {
        activeControls.getActiveComponent().find(".checkbox").height($(this).val());
    }
});

$borderWidth.on("change", function (e) {
    e.preventDefault();
    e.stopPropagation();

    if (activeControls.activeExists()) {
        activeControls.getActiveComponent().find(".checkbox").css("borderWidth", $(this).val());
    }
});

$borderColor.on("change", function (e) {
    e.preventDefault();
    e.stopPropagation();

    if (activeControls.activeExists()) {
        activeControls.getActiveComponent().find(".checkbox").css("borderColor", $(this).val());
    }
});

$board.on("click", function () {
    activeControls.removeActive();
});

addEventsToComponent($(".component"));

// $('html').keyup(function(e){
//     if(e.keyCode == 8) {
//         if (activeControls.activeExists()) {
//             activeControls.getActiveComponent().remove();
//         }
//     }
//
// }
//     );

$(window).on("keyup", function (e) {
    if (e.keyCode === 8) {
        if (activeControls.activeExists()) {
            activeControls.getActiveComponent().remove();
        }
    }
});

function addCheckboxComponent() {
    let $component = $(`<div class="component checkbox-component">
                    <label class="checkbox-container">
                        <input style="display: none;" type="checkbox">
                        <span class="checkbox"></span>
                    </label>
                    <div class="component-resize-controls">
                        <div class="resize-top-left resize-control"></div>
                        <div class="resize-top resize-control"></div>
                        <div class="resize-top-right resize-control"></div>
                        <div class="resize-right resize-control"></div>
                        <div class="resize-bottom-right resize-control"></div>
                        <div class="resize-bottom resize-control"></div>
                        <div class="resize-bottom-left resize-control"></div>
                        <div class="resize-left resize-control"></div>
                    </div>
                </div>`);

    addEventsToComponent($component);

    $board.append($component);
}

$("#add-checkbox").on("click", addCheckboxComponent);

addCheckboxComponent();
